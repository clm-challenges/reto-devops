#!/bin/bash

repository="aiaguila1"
appapi="reto-devops"
tag="v0.0.1"
nocache="--no-cache"

echo "Building API image"
echo docker build $nocache -t $repository/$appapi:$tag -f ../../Dockerfile --compress ../../
docker build $nocache -t $repository/$appapi:$tag -f ../../Dockerfile --compress ../../

# Requires docker login command previously
echo "Pushing API image"
echo docker push $repository/$appapi:$tag
docker push $repository/$appapi:$tag

echo ""
echo "Delete old images"
docker rmi $(docker images --filter dangling=true -q)

# docker run --rm -dp 3000:3000 aiaguila1/reto-devops:v0.0.1
# docker exec -it container_id bash