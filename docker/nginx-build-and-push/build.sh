#!/bin/bash

repository="aiaguila1"
appapi="nginx"
tag="v0.0.1"
nocache="--no-cache"

echo "Building API image"
echo docker build $nocache -t $repository/$appapi:$tag --compress .
docker build $nocache -t $repository/$appapi:$tag --compress .

# Requires docker login command previously
echo "Pushing API image"
echo docker push $repository/$appapi:$tag
docker push $repository/$appapi:$tag

echo ""
echo "Delete old images"
docker rmi $(docker images --filter dangling=true -q)

# docker run --rm -dp 80:80 aiaguila1/nginx:v0.0.1
# docker exec -it container_id bash