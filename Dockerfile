FROM alpine:3.13.6
ENV USER=node
### install dependencies & configure user
RUN apk add coreutils procps bash nodejs npm && \
    mkdir -p /home/${USER} && \
    adduser -h /home/${USER} -s /bin/bash -D ${USER} && \
    addgroup ${USER} users && \
    addgroup ${USER} wheel
USER ${USER}
### configure app
WORKDIR /home/${USER}
COPY . .
RUN npm install && \
    npm run test
EXPOSE 3000
CMD node index.js