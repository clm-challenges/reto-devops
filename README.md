# Reto-DevOps CLM
![CLM Consoltores](./img/clm.png)

Este reto fue diseñado para mostrar tus habilidades DevOps. Este repositorio contiene una aplicación simple en NodeJs.

¡¡Te damos la bienvenida al desafío de CLM Consultores!! Si estás en este proceso, es porque nos interesa que puedas ser parte de nuestro equipo.
## Fase del proceso de selección:
Antes de comenzar con el desafío, te recomendamos leer las siguientes instrucciones:
  1. Es importante que realices el reto en forma tranquila (tendrás 3 días máximo para poder enviarlo). No te preocupes sino puedes completar todas las fases, para nosotros es importante que realices lo que consideras que tienes experiencia.
  2. En caso de que cumplas con el perfil técnico del cargo, la segunda fase del proceso de selección es una entrevista técnica, en dónde validaremos tus conocimientos y podrás saber con mayor detalle las funciones asociadas al cargo y sobre el equipo del área.
  3. Si continúas avanzando con nosotros, el próximo paso es una entrevista Psicolaboral con el área de Gestión de Personas y posteriormente la coordinación del ingreso a la empresa.

Una vez completado, no olvide notificar la solución **a Carla Santiago csantiago@clmconsultores.com**

Si tienes alguna duda, puedes escribir a Carla Santiago o enviar un correo a Gestión de Personas rrhh@clmconsultores.com

¡Te deseamos mucho éxito!

## La app
![NodeJs](./img/nodejs.png)

```bash
$ git clone https://gitlab.com/clm-public/reto-devops.git
Cloning into 'reto-devops'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
$ cd ./reto-devops

```
### Instalar Dependencias
```bash
$ npm install
npm WARN basicservice@1.0.0 No repository field.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.1.2 (node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})

added 530 packages from 308 contributors and audited 1203947 packages in 34.589s

21 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```
### Ejecutar Test
![Jest](./img/jest.jpg)

```bash
$ npm run test

> basicservice@1.0.0 test /basic-unit-test
> jest

 PASS  tests/sum.test.js
 PASS  tests/string.test.js

Test Suites: 2 passed, 2 total
Tests:       3 passed, 3 total
Snapshots:   0 total
Time:        5.655s
Ran all test suites.
```

### Ejecutar la app
```bash
$ node index.js
Example app listening on port 3000!
```
Podrá acceder a la API localmente en el puerto `3000`.

```bash
$ curl -s localhost:3000/ | jq
{
  "msg": "ApiRest prueba"
}
$ curl -s localhost:3000/public | jq
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl -s localhost:3000/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

**NT:** En nuestro equipo consideramos que cada aplicación debe venir bien documentada por parte del desarrollador para que el equipo de **DevOps** puede realizar los procesos de automatización de una manera mas eficaz.

## El reto comienza aquí
Tienes que hacer un **fork** de este repositorio para completar los siguientes retos en tu propia cuenta de `gitlab`. **Siéntete libre de resolver el reto que desees.** La cantidad de retos resueltos nos va a permitir valorar tus habilidades y realizar una **oferta en base a las mismas**.

1. Una vez completado, no olvide notificar la solución al **Carla Santiago (csantiago@clmconsultores.com).**
2. **La solución debe venir bien documentada, ten en cuenta que vamos a ejecutar la solución que nos envies para realizar la evaluación**
3. **Tiempo de solución 3 días**

Si tiene alguna duda, adelante, [abre un issue](https://gitlab.com/clm-public/reto-devops/issues) para hacer cualquier pregunta sobre cualquier reto.

### Reto 1. Dockerize la aplicación
![docker](./img/nodedoker.jpg)

¿Qué pasa con los contenedores? En este momento **(2021)**, los contenedores son un estándar para implementar aplicaciones **(en la nube o en sistemas locales)**. Entonces, el reto es:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
2. Ejecutar la app como un usuario diferente de root.

## Comentarios

La aplicación se contenedorizó y se subió al [Registry Container público de Docker](https://hub.docker.com/)

El Dockerfile se puede revisar en el siguiente [link](https://gitlab.com/clm-challenges/reto-devops/-/blob/main/Dockerfile)

Si desea ejecutar la imagen en su PC local, debe ingresar el siguiente comando:

```bash
docker run --rm -dp 3000:3000 aiaguila1/reto-devops:v0.0.1
```

Si desea modificar la imagen, debe clonar este repositorio, posicionarse en el directorio './docker/api-build-and-push' y ejecutar el archivo 'build.sh'.

```bash
git clone https://gitlab.com/clm-challenges/reto-devops.git
cd ./docker/api-build-and-push
sh ./build.sh
```

### Reto 2. Docker Compose
![compose](./img/docker-compose.png)

Una vez que haya dockerizado todos los componentes de la API *(aplicación de NodeJS)*, estarás listo para crear un archivo docker-compose, en nuestro equipo solemos usarlo para levantar ambientes de desarrollo antes de empezar a escribir los pipeline. Ya que la aplicación se ejecuta sin ninguna capa para el manejo de protocolo http, añace:

1. Nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

## Comentarios

El docker-compose se puede revisar en el siguiente [link](https://gitlab.com/clm-challenges/reto-devops/-/blob/main/docker/docker-compose.yaml)

Todo lo relacionado a Docker, se encuentra en el directorio [docker](https://gitlab.com/clm-challenges/reto-devops/-/tree/main/docker) del repositorio.

Las credenciales para el endpoint /private son las siguientes:
- user-clm / Simple123

### Reto 3. Probar la aplicación en cualquier sistema CI/CD
![cicd](./img/cicd.jpg)

Como buen ingeniero devops, conoces las ventajas de ejecutar tareas de forma automatizada. Hay algunos sistemas de cicd que pueden usarse para que esto suceda. Elige uno, travis-ci, gitlab-ci, circleci ... lo que quieras. Danos una tubería exitosa. **Gitlab-CI** es nuestra herramienta de uso diario por lo cual obtendras puntos extras si usas gitlab.

## Comentarios

En el siguiente [link](https://gitlab.com/clm-challenges/reto-devops/-/blob/main/.gitlab-ci.yml) se encuentra la configuración del pipeline.
Acá vale la pena detenerse y hacer algunas acotaciones.

Gitlab provee varias funcionalidades que ayudan a los desarrolladores a mejorar la seguridad, calidad y rendimiento de sus aplicaciones.
Entre ellas está Auto DevOps. Para resolver este requerimiento utilicé una pipeline con estas plantillas. Auto DevOps provee pipelines realmente increíbles y quise presentarlo como una alternativa pipelines hechos a la antigua (cuando no existía Auto DevOps)

Cosas interesantes de este pipeline:
- Build & Push en el Registry de Contenedores de GitLab [link](https://gitlab.com/clm-challenges/reto-devops/container_registry/2329701)
- Ejecución de pruebas de aceptación
- Revisión de calidad del código
- Pruebas estáticas (SAST)
- Pruebas dinámicas (DAST). Deshabilité esto, ya que se requiere la configuración de un cluster.
- Revisión de secretos
- Escanéo de licencias
- Reporte de Vulnerabilidades [link](https://gitlab.com/clm-challenges/reto-devops/-/security/vulnerability_report)
- Manejo de Issues
- y un largo etcétera.

La verdad que GitLab es una plataforma muy potente para las organizaciones que quieren acelerar sus procesos de desarrollo de software.
He tenido la suerte de estudiar bastante sobre sus bondades.

### Reto 4. Deploy en kubernetes
![k8s](./img/k8s.png)

Ya que eres una máquina creando contenedores, ahora queremos ver tu experiencia en k8s. Use un sistema kubernetes para implementar la API. Recomendamos que uses herramientas como Minikube o Microk8s.

Escriba el archivo de implementación (archivo yaml) utilizalo para implementar su API (aplicación Nodejs).

* añade **Horizontal Pod Autoscaler** a la app NodeJS

## Comentarios

En el siguiente [link](https://gitlab.com/clm-challenges/reto-devops/-/tree/main/k8s) se encuentra todo lo relacionado con kubernetes.

En el deployment aproveché la imagen que subí al registry de Gitlab, la cual de aquí en adelente debe ser la fuente de la verdad.

El archivo 'reto-devops.yaml' implementa lo requerido. Lo probé en un cluster de GCP. (suscripción gratis)

Para probar la implementación, se debe clonar el repositorio (si es que no se ha hecho antes), loguearse en el cluster y ejecutar los siguientes comandos:

```bash
git clone https://gitlab.com/clm-challenges/reto-devops.git
cd ./k8s
kubectl create ns devops
kubectl apply -f reto-devops.yaml -n devops
```

### Reto 5. Construir Chart en helm y manejar trafico http(s)
![helm](./img/helm-logo-1.jpg)

Realmente el pan de cada día es crear, modificar y usar charts de helm. Este reto consiste en:

1. Diseñar un chart de helm con nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

## Comentarios

El Chart se publicó en el siguiente repositorio [GitHub](https://alex-aguila.github.io/helmcharts/)

Para instalar nginx con la configuración requerida, debe estar logueado en el cluster y haber instalado previamente la app node. Luego ejecutar:

```bash
helm repo add nginxchart https://alex-aguila.github.io/helmcharts/
helm repo update
helm install nginxchart -namespace devops https://alex-aguila.github.io/helmcharts/ 
```


### Reto 6. Terraform
![docker](./img/tf.png)

En estos días en IaC no se habla de más nada que no sea terraform, en **CLM** ya nos encontramos con pipeline automatizados de Iac. El reto consiste en crear un modulo de terraform que nos permita crear un **rbac.authorization de tipo Role** que solo nos permita ver los pods de nuestro **namespace donde se encuentra al app Nodejs**

## Comentarios

El módulo Terraform que cumple con lo solicitado está en la siguiente [ruta](https://gitlab.com/clm-challenges/module-rbac)
El módulo es audocumentado con Terraform-docs.

Si desea probar su funcionamiento, puede importarlo desde su repositorio GitLab, que es uno de los lugares donde podemos subir móduloes Terraform

```terraform
data "google_client_config" "provider" {}

provider "google" {
  project = var.project
  region  = var.region
}

module "rbac" {
  source    = "git::https://gitlab.com/clm-challenges/module-rbac.git"
  namespace = var.namespace
}
```


### Reto 7. Automatiza el despliegue de los retos realizados
![docker](./img/make.gif)

Ya que hoy en día no queremos recordar recetas ni comandos, el reto consiste en **automatizar los retos en un Makefile**, considera especificar cuales son las dependencias necesarias para que tu Makefile se ejecute sin problemas.

## Comentarios

Por falta de tiempo no alcancé a hacer este reto, auque tengo experiencia armando Makefile para ejecutar pipelines en Jenkins (hace 2 años atrás)
Hay una cantidad importante de cosas que se pueden hacer de forma mucho más profesional y claramente ustedes lo saben.
He trabajado en varios lugares y acá se vé que hay bastante conocimiento.

Saludos muchach@s!

**NT:** Se evaluará el orden en el cual se encuentre el repositorio, en el gran universo de aplicaciones que existe hoy en día el orden es un factor importante.
